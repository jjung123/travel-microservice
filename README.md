https://github.com/openfaas/faasd/blob/master/docs/MULTIPASS.md


# boot the VM using the cloud-config.txt with my own SSH public key
cd
multipass delete --purge virtcloud
multipass launch --cloud-init cloud-config.txt  --name virtcloud
multipass info virtcloud

# test connection
echo $IP
ssh ubuntu@$IP
logout