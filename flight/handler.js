/*
  A RESTful microservice that consumes RapidAPI"s Skyscanner flight information search service, and sends a JSON
  data containing the cached airfare that it found.
*/

"use strict"

import request from "request"

const URL = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsequotes/v1.0/US/USD/en-US/"

module.exports = async (event, context) => {

  // Configure the HTTP headers for access to the external API
  const options = {
    method: "GET",
    url: URL + event.query.orig + "-sky/" + event.query.dest + "-sky/" + Date.parse(event.query.date),
    qs: { inboundpartialdate: Date.parse(event.query.date) },
    headers: {
      "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
      "x-rapidapi-key": "f6cbdad16cmshd44a283796d0457p1f652cjsn1a4ba453a060",
      useQueryString: true
    }
  }

  const getAirfare = (options) => {
    request(options, (error, response, body) => {
      if (error) throw new Error(error)
      result = {
        "flight": JSON.stringify(body)
      }
    })
    return result
  }

  return context
    .status(200)
    .succeed(getAirfare(options))
}